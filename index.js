var myApp = angular.module('myApp', []);

angular.module('myApp').directive('content', function() {
    return {
        restrict: 'E',
        template: `<p>{{a}}</p><p>{{b}}</p><p>{{text}}</p><input type="button" id="counter">`,
        transclude: true,
        scope: {
            b: '=',
            text: '='
        },
        link: function (scope) {
            scope.a = 1;
            scope.b = 100;
            scope.text = "Welcome!";
            /*scope.onClick = function () {
                scope.a++;
            }*/
        $('#counter').click(function() {
            scope.$apply(function () {
                scope.a++;
            });
        });
        }
    };
});

myApp.controller("MyController", function($scope, $timeout, $http){
    setTimeout(function(){
        $scope.$apply(function () {
        $scope.bbb++;
        $.ajax({
            url: "https://baconipsum.com/api/?type=all-meat&paras=1",
            success: function(result) {
                $scope.$apply(function () {
                $scope.newText=result[0];
                });
            }
        });
        });
        }, 2000);

});

